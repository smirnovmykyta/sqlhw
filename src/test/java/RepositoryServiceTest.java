import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.RepositoryService;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class RepositoryServiceTest {

    @BeforeEach
    public void connectToDB() {
        RepositoryService.connectToDatabase();
    }

    @Test
    void amountOfPeople_ShouldCountAmountOfAllPeopleInDatabase() {

        //given
        int expected = 138;
        int expected1 = 0;

        //when
        int actual = RepositoryService.amountOfPerson();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual);
    }

    @Test
    void averageAgeOfPeople_ShouldCountAnAverageAgeOfPeopleInDatabase() {

        //given
        int expected = 32;
        int expected1 = 0;

        //when
        int actual = RepositoryService.averageAgeOfPerson();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual);
    }

    @Test
    void sortedListOfLastName_ShouldShowSortedListOfLastNames() {

        //given
        String expected = "[Барберов, Бондаренко, Бульба, Водкин, ДайВинчик, Зинчук, Кичук, Корнев, Медяник, Олейникова, Остапчук, Павлова, Петренко, Пупкин, Салун, Семенюк, Смирнов, Соболева, Таптунов, Филатов, Цикаленко, Черняков]";
        List<String> expected1 = null;

        //when
        List<String> actual = RepositoryService.sortOfLastNamePerson();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void sortedListOfLastNameAndAmountOfPeopleWithSameLastName_ShouldShowSortedListOfLastNamesAndAmountOfPeopleWithSameLastName() {

        //given
        String expected = "{Салун=6, Медяник=6, Кичук=6, Таптунов=6, Бондаренко=6, Остапчук=6, Цикаленко=6, Водкин=6, Бульба=6, Барберов=6, Семенюк=6, Соболева=6, Петренко=6, Зинчук=6, Пупкин=6, Олейникова=6, ДайВинчик=12, Павлова=6, Корнев=6, Филатов=6, Черняков=6, Смирнов=6}";
        List<String> expected1 = null;

        //when
        Map<String, String> actual = RepositoryService.amountPersonWhithIdenticallyLastName();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfLastNamesWithLetterB_ShouldShowListOfLastNamesWithLetterB() {

        //given
        String expected = "[Соболева, Бульба, Барберов, Соболева, Бульба, Барберов, Соболева, Бульба, Барберов, Соболева, Бульба, Барберов, Соболева, Бульба, Барберов, Соболева, Бульба, Барберов]";
        List<String> expected1 = null;

        //when
        List<String> actual = RepositoryService.letterBInLastName();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfPeopleWithoutAdress_ShouldShowListOfPeopleWithoutAdress() {

        //given
        String expected = "[\nid: 11, Влад Бондаренко, возраст 27, место жительство  null, \n" +
                "id: 12, Виталина Зинчук, возраст 41, место жительство  null, \n" +
                "id: 23, Люцио ДайВинчик, возраст 23, место жительство  null, \n" +
                "id: 34, Влад Бондаренко, возраст 27, место жительство  null, \n" +
                "id: 35, Виталина Зинчук, возраст 41, место жительство  null, \n" +
                "id: 46, Люцио ДайВинчик, возраст 23, место жительство  null, \n" +
                "id: 57, Влад Бондаренко, возраст 27, место жительство  null, \n" +
                "id: 58, Виталина Зинчук, возраст 41, место жительство  null, \n" +
                "id: 69, Люцио ДайВинчик, возраст 23, место жительство  null, \n" +
                "id: 80, Влад Бондаренко, возраст 27, место жительство  null, \n" +
                "id: 81, Виталина Зинчук, возраст 41, место жительство  null, \n" +
                "id: 92, Люцио ДайВинчик, возраст 23, место жительство  null, \n" +
                "id: 103, Влад Бондаренко, возраст 27, место жительство  null, \n" +
                "id: 104, Виталина Зинчук, возраст 41, место жительство  null, \n" +
                "id: 115, Люцио ДайВинчик, возраст 23, место жительство  null, \n" +
                "id: 126, Влад Бондаренко, возраст 27, место жительство  null, \n" +
                "id: 127, Виталина Зинчук, возраст 41, место жительство  null, \n" +
                "id: 138, Люцио ДайВинчик, возраст 23, место жительство  null]";
        List<String> expected1 = null;

        //when
        List<String> actual = RepositoryService.personWhithoutPlaceOfResidence();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfPeopleWhoLiveOnProspectPravdyUnderEighteen_ShouldShowPeopleWhoLiveOnProspectPravdyAndHaveUnderEighteenYearsOld() {

        //given
        String expected = "[]";
        List<String> expected1 = null;

        //when
        List<String> actual = RepositoryService.minorsPersonOfProspectPravdi();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfStreetsWithAmountOfPeople_ShouldShowListOfStreetsWithAmountOfPeople() {

        //given
        String expected = "{Вербовая=12, Валковская=12, Волгоградская=6, Академика Горбунова=12, Батума=18, проспект Правды=18, Васильковская=6, Газовая=12, Западинская=12, Карьерная=12}";
        List<String> expected1 = null;

        //when
        Map<String, String> actual = RepositoryService.sortStreetNameWhithAmountOfPerson();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfStreetsWithSixLetters_ShouldShowListOfStreetsWithSixLetters() {

        //given
        String expected = "[]";
        List<String> expected1 = null;

        //when
        List<String> actual = RepositoryService.sixLettersStreet();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }

    @Test
    void listOfStreetsWithLessThanThreeResidents_ShouldShowListOfStreetsWithLessThanThreeResidents() {

        //given
        String expected = "[]";
        List<String> expected1 = null;

        //when
        List<String> actual = RepositoryService.streetLessThanThreePerson();

        //then
        assertEquals(expected, actual.toString());
        assertNotEquals(expected1, actual.toString());
    }
}
