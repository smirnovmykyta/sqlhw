package service;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepositoryService {
    private static final String url = "jdbc:mysql://127.0.0.1:3306/persons?useUnicode=true&serverTimezone=UTC";

    private static String username = "root";
    private static String password = "root";
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    public static void connectToDatabase() {
        try {

            connection = DriverManager.getConnection(url, username, password);

            statement = connection.createStatement();

        } catch (SQLException sqlEx) {
            System.out.println("Connection Failed! Check output console");
            sqlEx.printStackTrace();
        }
    }

    public static void insertDefaultPersons() throws SQLException {
        String queryForPeople = "INSERT INTO persons.persons (firstName, lastName , age, idStreet) \n" +
                " VALUES ('Никита', 'Смирнов',22,1),('Ярослав', 'Семенюк',20,2),('Сергей', 'Кичук',25,3),('Никита', 'Цикаленко',22,4)," +
                "('Саша', 'Салун',35,5),('Коля', 'Медяник',10,6),('Вася', 'Пупкин',62,7),('Петр', 'Водкин',78,8)," +
                "('Вадим', 'Корнев',42,9),('Анна', 'Соболева',26,10),('Влад', 'Бондаренко',27,NULL),('Виталина', 'Зинчук',41,NULL)," +
                "('Валерий', 'Остапчук',30,3),('Валентин', 'Черняков',35,4),('Леонардо', 'ДайВинчик',36,5)," +
                "('Евгений', 'Филатов',24,2),('Евгений', 'Таптунов',23,3),('Марина', 'Олейникова',15,7),('Катерина', 'Петренко',16,8)," +
                "('Катерина', 'Павлова',19,10),('Марк', 'Бульба',30,9),('Олег', 'Барберов',95,8),('Люцио', 'ДайВинчик',23,NULL);";

        String queryForStreet = "INSERT INTO persons.street (streetName) VALUES ('Васильковская'),('Академика Горбунова')," +
                "('Батума'),('Валковская'),('Вербовая'),('Волгоградская'),('Газовая'),('проспект Правды'),('Западинская'),('Карьерная');";

        statement.executeUpdate(queryForStreet);
        statement.executeUpdate(queryForPeople);
    }

    public static int amountOfPerson() {
        String query = "SELECT COUNT(*) FROM persons";
        int count = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static int averageAgeOfPerson() {
        String query = "SELECT avg(age) FROM persons";
        int count = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static List<String> sortOfLastNamePerson() {
        String query = "SELECT DISTINCT lastName FROM persons ORDER BY lastName ASC";
        List<String> listOfPerson = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                listOfPerson.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfPerson;
    }

    public static Map<String, String> amountPersonWhithIdenticallyLastName() {
        String query = "SELECT lastName, COUNT(*) FROM persons GROUP BY lastName";
        Map<String, String> mapLastName = new HashMap<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {

                mapLastName.put(resultSet.getString(1), resultSet.getString(2));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mapLastName;
    }

    public static List<String> letterBInLastName() {
        String query = "SELECT lastName FROM persons WHERE lastName LIKE '_%б%_'";
        List<String> listLastName = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                listLastName.add(resultSet.getString(1));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listLastName;
    }

    public static List<String> personWhithoutPlaceOfResidence() {
        String query = "SELECT * FROM persons WHERE idStreet IS NULL";
        List<String> listOfBum = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String persons = "";
                persons = "\nid: " + resultSet.getString(1) + ", " + resultSet.getString(2) + " " +
                        resultSet.getString(3) + ", возраст " + resultSet.getString(4) +
                        ", место жительство  " + resultSet.getString(5);
                listOfBum.add(persons);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOfBum;
    }

    public static List<String> minorsPersonOfProspectPravdi() {
        String query = "SELECT * FROM persons JOIN street ON persons.idStreet = street.idStreet\n" +
                "WHERE  UPPER(streetName) = '%проспект Правды%' AND persons.age < 18";
        List<String> listOFPerson = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String persons = "";
                persons = "id: " + resultSet.getString(1) + " " + resultSet.getString(2) + " " +
                        resultSet.getString(3) + ", возраст " + resultSet.getString(4);
                listOFPerson.add(persons);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listOFPerson;
    }


    public static Map<String, String> sortStreetNameWhithAmountOfPerson() {
        String query = "SELECT streetName, COUNT(idPersons) FROM street JOIN persons ON street.idStreet = persons.idStreet " +
                "GROUP BY streetName";
        Map<String, String> mapSteet = new HashMap<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                mapSteet.put(resultSet.getString(1), resultSet.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mapSteet;
    }

    public static List sixLettersStreet() {
        String query = "SELECT * FROM street WHERE LENGTH(streetName) = 6";
        List<String> listSteerWhithSixLetters = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                listSteerWhithSixLetters.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listSteerWhithSixLetters;
    }

    public static List<String> streetLessThanThreePerson() {
        String query = "SELECT streetName FROM street JOIN persons ON street.idStreet = persons.idStreet" +
                " GROUP BY streetName" +
                " HAVING COUNT(persons.idPersons) < 3";
        List<String> listStreet = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                listStreet.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listStreet;
    }
}
