import service.RepositoryService;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {

        RepositoryService.connectToDatabase();

//        RepositoryService.insertDefaultPersons();

        System.out.println("Количество людей = " + RepositoryService.amountOfPerson());
        System.out.println("Средний возраст людей = " + RepositoryService.averageAgeOfPerson());
        System.out.println("Упорядоченый список фамилий: " + RepositoryService.sortOfLastNamePerson());
        System.out.println("Список фамилий и количество людей с данной фамилией: " +
                RepositoryService.amountPersonWhithIdenticallyLastName());
        System.out.println("Фамилии имеющие в середине букву \"б\": " + RepositoryService.letterBInLastName());
        System.out.println("Люди без определенного места жительства: " + RepositoryService.personWhithoutPlaceOfResidence());
        System.out.println("Список несовершенно летних людей живущих на проспекте Правды: " +
                RepositoryService.minorsPersonOfProspectPravdi());
        System.out.println("Упорядоченный список улиц с колицестов жильцов на каждой уличе: " +
                RepositoryService.sortStreetNameWhithAmountOfPerson());
        System.out.println("Список улиц из 6 букв: " + RepositoryService.sixLettersStreet());
        System.out.println("Список улиц на которых живет меньше 3 людей: " + RepositoryService.streetLessThanThreePerson());
    }
}
